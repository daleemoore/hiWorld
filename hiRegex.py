# TODO, test Brian Evans and his daughter's regex!

import re

"""
Brian's question:
- - - - - - - - -
This is python oriented, but if it can be solved completely in regex that is best. If some python is used (with regex), that is ok too.

Given these strings.

str2q = 'two questions? Right?'
str3q = 'hello? how are you?? fine? ya'
str3aq = 'end? with?? question mark?'
str4q = 'boo? ya?? really four? questions? eh'
str4aq = boo? ya?? really four? question mark end?'

Detect whether a string has exactly 3 questions or not. Of the above strings, only str3q and str3aq should match.

The regex construct {3} is supposed to detect 'exactly 3' of the thing preceding it. For example, a regex of 'a{3}' should detect three consecutive letter 'a'. The problem is, in a given string, it detects the last (3 in this case) occurrence. So if the string is 'baaaa' which has four 'a', since there are three in a row (the first three, then the last 3), it matches. We want to detect over the entire string that there are only 3 questions (and as you can see, a question might end in more than one question mark, so counting question marks is out, and the test strings above are examples, so a question could have more than one or two question marks).

Here is what we tried. Regex (in python):  r'(.*\?+){3}'

Using regex101.com we tried variations of that but it would match when there were more than 3 questions. I also tried some  python solutions like len(match.groups() ) etc (using python re module; you have to use regex...this is a problem in my daughter's homework).
"""

""" 
Brian's clarification
- - - - - - - - - - -
A question is one or more words followed by one or more question marks.

Questions:
"eh?"
"boo yah??"
"then they told me, wtf???  no. Are you serious?"  (<-- two questions)

NOT a question:
"?  ?  "

A question followed by NOT a question:
"Hello there? ?"
"""

""" 
Joey's answer
- - - - - - -
The issue is that regex is typically greedy, so (.*\?+) 
The '.' will match ?, So you need to be explicit.  
(I could be wrong, and an online regexp match tool might show you what matched each group.

(([^\?]+){3})

You also need the wrapper around the whole thing, since you care about the whole match, not partial.

If you don't want to match anything with extra goo at the end, you need to nail it down with a $ at the end.

(([^\?]+\?+){3}$)
"""

def main():
    print("Hello world!")
    ar = [
        'two questions? Right?',
        'hello? how are you?? fine? ya',
        'end? with?? question mark?',
        'boo? ya?? really four? questions? eh',
        'boo? ya?? really four? question mark end?',
        'then they told me, wtf???  no. Are you serious?',
    ]
    for line in ar:
        print("the line:{}".format(line))
        result = re.search(r'(([ ^\?]+){3})',line, re.M | re.I)
        # (([^\?]+){3})
        # (([^\?]+\?+){3}$)
        if result:
            print('3 Questions found.')
        else:
            print('3 Questions NOT found.')


if __name__ == "__main__":
    main()
